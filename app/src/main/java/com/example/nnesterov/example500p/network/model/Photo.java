package com.example.nnesterov.example500p.network.model;

import java.util.List;

public class Photo {
    private int id;
    private String name;
    private User user;
    private List<ImageUrl> images;

    public Photo() {}

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ImageUrl> getImages() {
        return images;
    }

    public void setImages(List<ImageUrl> images) {
        this.images = images;
    }
}
