package com.example.nnesterov.example500p.data;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.example.nnesterov.example500p.BuildConfig;
import com.example.nnesterov.example500p.data.photo.PhotoColumns;
import com.example.nnesterov.example500p.data.user.UserColumns;

public class PhotosSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = PhotosSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_FILE_NAME = "photos.db";
    private static final int DATABASE_VERSION = 1;
    private static PhotosSQLiteOpenHelper sInstance;
    private final Context mContext;
    private final PhotosSQLiteOpenHelperCallbacks mOpenHelperCallbacks;

    // @formatter:off
    public static final String SQL_CREATE_TABLE_PHOTO = "CREATE TABLE IF NOT EXISTS "
            + PhotoColumns.TABLE_NAME + " ( "
            + PhotoColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PhotoColumns.PHOTO_ID + " INTEGER, "
            + PhotoColumns.CATEGORY + " TEXT, "
            + PhotoColumns.NAME + " TEXT, "
            + PhotoColumns.USER_ID + " INTEGER, "
            + PhotoColumns.SMALL_IMAGE_URL + " TEXT, "
            + PhotoColumns.LARGE_IMAGE_URL + " TEXT "
            + ", CONSTRAINT unique_photo_id UNIQUE (photo_id) ON CONFLICT REPLACE"
            + " );";

    public static final String SQL_CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS "
            + UserColumns.TABLE_NAME + " ( "
            + UserColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UserColumns.USER_ID + " INTEGER, "
            + UserColumns.NAME + " TEXT, "
            + UserColumns.CITY + " TEXT, "
            + UserColumns.COUNTRY + " TEXT "
            + ", CONSTRAINT unique_user_id UNIQUE (user_id) ON CONFLICT REPLACE"
            + " );";

    // @formatter:on

    public static PhotosSQLiteOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = newInstance(context.getApplicationContext());
        }
        return sInstance;
    }

    private static PhotosSQLiteOpenHelper newInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return newInstancePreHoneycomb(context);
        }
        return newInstancePostHoneycomb(context);
    }


    /*
     * Pre Honeycomb.
     */
    private static PhotosSQLiteOpenHelper newInstancePreHoneycomb(Context context) {
        return new PhotosSQLiteOpenHelper(context);
    }

    private PhotosSQLiteOpenHelper(Context context) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        mOpenHelperCallbacks = new PhotosSQLiteOpenHelperCallbacks();
    }


    /*
     * Post Honeycomb.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static PhotosSQLiteOpenHelper newInstancePostHoneycomb(Context context) {
        return new PhotosSQLiteOpenHelper(context, new DefaultDatabaseErrorHandler());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private PhotosSQLiteOpenHelper(Context context, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION, errorHandler);
        mContext = context;
        mOpenHelperCallbacks = new PhotosSQLiteOpenHelperCallbacks();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        mOpenHelperCallbacks.onPreCreate(mContext, db);
        db.execSQL(SQL_CREATE_TABLE_PHOTO);
        db.execSQL(SQL_CREATE_TABLE_USER);
        mOpenHelperCallbacks.onPostCreate(mContext, db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            setForeignKeyConstraintsEnabled(db);
        }
        mOpenHelperCallbacks.onOpen(mContext, db);
    }

    private void setForeignKeyConstraintsEnabled(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setForeignKeyConstraintsEnabledPreJellyBean(db);
        } else {
            setForeignKeyConstraintsEnabledPostJellyBean(db);
        }
    }

    private void setForeignKeyConstraintsEnabledPreJellyBean(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setForeignKeyConstraintsEnabledPostJellyBean(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mOpenHelperCallbacks.onUpgrade(mContext, db, oldVersion, newVersion);
    }
}
