package com.example.nnesterov.example500p.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nnesterov.example500p.R;
import com.example.nnesterov.example500p.data.photo.PhotoColumns;
import com.example.nnesterov.example500p.data.photo.PhotoCursor;
import com.example.nnesterov.example500p.data.user.UserColumns;
import com.example.nnesterov.example500p.data.user.UserCursor;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PhotoDetailsFragment extends Fragment {
    public static final String ARG_PHOTO_ID = "item_id";

    @InjectView(R.id.photo_name_view)
    TextView photoNameView;

    @InjectView(R.id.photo_view)
    ImageView photoView;

    private int photoId;

    public PhotoDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_PHOTO_ID)) {
            photoId = getArguments().getInt(ARG_PHOTO_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_item, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showContent();
    }

    private void showContent() {
        PhotoCursor photoCursor = getPhotoCursor();
        if (photoCursor.moveToFirst()) {
            ImageLoader.getInstance().displayImage(photoCursor.getLargeImageUrl(), photoView);
            StringBuilder photoInfoBuilder = new StringBuilder();
            photoInfoBuilder.append(photoCursor.getName());
            int userId = photoCursor.getUserId();
            UserCursor userCursor = getUserCursor(userId);
            if (userCursor.moveToFirst()) {
                photoInfoBuilder.append("\r\n")
                        .append(userCursor.getName())
                        .append("\r\n")
                        .append(userCursor.getCity())
                        .append("\r\n")
                        .append(userCursor.getCountry());
            }
            photoNameView.setText(photoInfoBuilder.toString());
        }
        photoCursor.close();
    }

    private PhotoCursor getPhotoCursor() {
        Context context = getActivity();
        ContentResolver resolver = context.getContentResolver();
        PhotoCursor photoCursor = new PhotoCursor(resolver.query(PhotoColumns.CONTENT_URI,
                null,
                PhotoColumns.PHOTO_ID + "=?",
                new String[]{String.valueOf(photoId)},
                null));

        return photoCursor;
    }

    private UserCursor getUserCursor(int userId) {
        Context context = getActivity();
        ContentResolver resolver = context.getContentResolver();
        UserCursor userCursor = new UserCursor(resolver.query(UserColumns.CONTENT_URI,
                null,
                UserColumns.USER_ID + "=?",
                new String[]{String.valueOf(userId)}
                ,null));

        return userCursor;
    }

}
