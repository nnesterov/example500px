package com.example.nnesterov.example500p.data.photo;

import java.util.Date;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.example.nnesterov.example500p.data.base.AbstractSelection;

/**
 * Selection for the {@code photo} table.
 */
public class PhotoSelection extends AbstractSelection<PhotoSelection> {
    @Override
    protected Uri baseUri() {
        return PhotoColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code PhotoCursor} object, which is positioned before the first entry, or null.
     */
    public PhotoCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new PhotoCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null)}.
     */
    public PhotoCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null)}.
     */
    public PhotoCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public PhotoSelection id(long... value) {
        addEquals("photo." + PhotoColumns._ID, toObjectArray(value));
        return this;
    }

    public PhotoSelection photoId(Integer... value) {
        addEquals(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoSelection photoIdNot(Integer... value) {
        addNotEquals(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoSelection photoIdGt(int value) {
        addGreaterThan(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoSelection photoIdGtEq(int value) {
        addGreaterThanOrEquals(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoSelection photoIdLt(int value) {
        addLessThan(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoSelection photoIdLtEq(int value) {
        addLessThanOrEquals(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoSelection category(String... value) {
        addEquals(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoSelection categoryNot(String... value) {
        addNotEquals(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoSelection categoryLike(String... value) {
        addLike(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoSelection categoryContains(String... value) {
        addContains(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoSelection categoryStartsWith(String... value) {
        addStartsWith(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoSelection categoryEndsWith(String... value) {
        addEndsWith(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoSelection name(String... value) {
        addEquals(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoSelection nameNot(String... value) {
        addNotEquals(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoSelection nameLike(String... value) {
        addLike(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoSelection nameContains(String... value) {
        addContains(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoSelection nameStartsWith(String... value) {
        addStartsWith(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoSelection nameEndsWith(String... value) {
        addEndsWith(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoSelection userId(Integer... value) {
        addEquals(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoSelection userIdNot(Integer... value) {
        addNotEquals(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoSelection userIdGt(int value) {
        addGreaterThan(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoSelection userIdGtEq(int value) {
        addGreaterThanOrEquals(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoSelection userIdLt(int value) {
        addLessThan(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoSelection userIdLtEq(int value) {
        addLessThanOrEquals(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoSelection smallImageUrl(String... value) {
        addEquals(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection smallImageUrlNot(String... value) {
        addNotEquals(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection smallImageUrlLike(String... value) {
        addLike(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection smallImageUrlContains(String... value) {
        addContains(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection smallImageUrlStartsWith(String... value) {
        addStartsWith(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection smallImageUrlEndsWith(String... value) {
        addEndsWith(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection largeImageUrl(String... value) {
        addEquals(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection largeImageUrlNot(String... value) {
        addNotEquals(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection largeImageUrlLike(String... value) {
        addLike(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection largeImageUrlContains(String... value) {
        addContains(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection largeImageUrlStartsWith(String... value) {
        addStartsWith(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }

    public PhotoSelection largeImageUrlEndsWith(String... value) {
        addEndsWith(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }
}
