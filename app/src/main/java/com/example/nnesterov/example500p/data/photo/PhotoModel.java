package com.example.nnesterov.example500p.data.photo;

import com.example.nnesterov.example500p.data.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code photo} table.
 */
public interface PhotoModel extends BaseModel {

    /**
     * Get the {@code photo_id} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getPhotoId();

    /**
     * Get the {@code category} value.
     * Can be {@code null}.
     */
    @Nullable
    String getCategory();

    /**
     * Get the {@code name} value.
     * Can be {@code null}.
     */
    @Nullable
    String getName();

    /**
     * Get the {@code user_id} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getUserId();

    /**
     * Get the {@code small_image_url} value.
     * Can be {@code null}.
     */
    @Nullable
    String getSmallImageUrl();

    /**
     * Get the {@code large_image_url} value.
     * Can be {@code null}.
     */
    @Nullable
    String getLargeImageUrl();
}
