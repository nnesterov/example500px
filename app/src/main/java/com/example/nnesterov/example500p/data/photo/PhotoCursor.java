package com.example.nnesterov.example500p.data.photo;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nnesterov.example500p.data.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code photo} table.
 */
public class PhotoCursor extends AbstractCursor implements PhotoModel {
    public PhotoCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(PhotoColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code photo_id} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getPhotoId() {
        Integer res = getIntegerOrNull(PhotoColumns.PHOTO_ID);
        return res;
    }

    /**
     * Get the {@code category} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getCategory() {
        String res = getStringOrNull(PhotoColumns.CATEGORY);
        return res;
    }

    /**
     * Get the {@code name} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getName() {
        String res = getStringOrNull(PhotoColumns.NAME);
        return res;
    }

    /**
     * Get the {@code user_id} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getUserId() {
        Integer res = getIntegerOrNull(PhotoColumns.USER_ID);
        return res;
    }

    /**
     * Get the {@code small_image_url} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getSmallImageUrl() {
        String res = getStringOrNull(PhotoColumns.SMALL_IMAGE_URL);
        return res;
    }

    /**
     * Get the {@code large_image_url} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getLargeImageUrl() {
        String res = getStringOrNull(PhotoColumns.LARGE_IMAGE_URL);
        return res;
    }
}
