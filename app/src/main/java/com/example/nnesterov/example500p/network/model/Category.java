package com.example.nnesterov.example500p.network.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Category {

    public static List<Category> ITEMS = new ArrayList<>();

    public static Map<String, Category> ITEM_MAP = new HashMap<>();

    static {
        addItem(new Category("0", "Uncategorized"));
        addItem(new Category("10", "Abstract"));
        addItem(new Category("11", "Animals"));
        addItem(new Category("5", "Black and White"));
        addItem(new Category("1", "Celebrities"));
        addItem(new Category("9", "City and Architecture"));
        addItem(new Category("15", "Commercial"));
        addItem(new Category("16", "Concert"));
        addItem(new Category("20", "Family"));
        addItem(new Category("14", "Fashion"));
        addItem(new Category("2", "Film"));
        addItem(new Category("24", "Fine Art"));
        addItem(new Category("23", "Food"));
        addItem(new Category("3", "Journalism"));
        addItem(new Category("8", "Landscapes"));
        addItem(new Category("12", "Macro"));
        addItem(new Category("18", "Nature"));
        addItem(new Category("4", "Nude"));
        addItem(new Category("7", "People"));
        addItem(new Category("19", "Performing Arts"));
        addItem(new Category("17", "Sport"));
        addItem(new Category("6", "Still Life"));
        addItem(new Category("21", "Street"));
        addItem(new Category("26", "Transportation"));
        addItem(new Category("13", "Travel"));
        addItem(new Category("22", "Underwater"));
        addItem(new Category("27", "Urban Exploration"));
        addItem(new Category("25", "Wedding"));
    }

    private static void addItem(Category item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.getId(), item);
    }

    private final String id;
    private final String name;

    private Category(String id, String name) {
        this.id = id;
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
