package com.example.nnesterov.example500p.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nnesterov.example500p.R;
import com.example.nnesterov.example500p.data.photo.PhotoColumns;
import com.example.nnesterov.example500p.data.photo.PhotoCursor;
import com.example.nnesterov.example500p.network.PhotoService;
import com.example.nnesterov.example500p.network.model.Category;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class CategoryDetailFragment extends Fragment {
    public static final String ARG_ITEM_ID = "item_id";
    public static final int COLUMN_COUNT = 3;

    private static Callbacks dummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(int id) {

        }
    };


    private Category item;

    @InjectView(R.id.photos_view)
    RecyclerView photosView;

    @InjectView(R.id.category_detail)
    TextView categoryName;


    private PhotoCursorAdapter adapter;
    private ContentObserver observer;
    private Callbacks callbacks = dummyCallbacks;


    public CategoryDetailFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        callbacks = (Callbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            item = Category.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_detail, container, false);
        if (item != null) {
            ButterKnife.inject(this, rootView);
            categoryName.setText(item.getName());
            photosView.setLayoutManager(new GridLayoutManager(getActivity(), COLUMN_COUNT));
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        observer = new PhotoObserver(getView().getHandler());
        getActivity().getContentResolver().registerContentObserver(PhotoColumns.CONTENT_URI, true, observer);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (observer != null) {
            getActivity().getContentResolver().unregisterContentObserver(observer);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showContent();
        requestUpdate();
    }

    private void showContent() {

        Cursor cursor = getActivity().getContentResolver().query(PhotoColumns.CONTENT_URI,
                null,
                PhotoColumns.CATEGORY + "=?",
                new String[]{item.getName()},
                null);
        if (adapter == null) {
            adapter = new PhotoCursorAdapter(cursor, getActivity());
        } else {
            adapter.changeCursor(cursor);
        }
        photosView.setAdapter(adapter);
    }

    private void requestUpdate() {
        Intent request = new Intent(getActivity(), PhotoService.class);
        request.putExtra(PhotoService.REQUEST_EXTRA, PhotoService.REQUEST_PHOTO_LIST);
        request.putExtra(PhotoService.CATEGORY_NAME_EXTRA, item.getName());
        getActivity().startService(request);
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.photo_name_view)
        TextView photoNameView;

        @InjectView(R.id.photo_view)
        ImageView photoView;

        int photoId;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callbacks.onItemSelected(photoId);
                }
            });
        }
    }

    private class PhotoCursorAdapter extends CursorRecyclerAdapter<PhotoViewHolder> {
        private final Context context;
        private final LayoutInflater layoutInflater;

        public PhotoCursorAdapter(Cursor cursor, Context context) {
            super(cursor);
            this.context = context;
            this.layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public void onBindViewHolderCursor(PhotoViewHolder holder, Cursor cursor) {
            PhotoCursor photoCursor = new PhotoCursor(cursor);
            holder.photoNameView.setText(photoCursor.getName());
            holder.photoView.setImageResource(0);
            holder.photoId = photoCursor.getPhotoId();
            ImageLoader.getInstance().displayImage(photoCursor.getSmallImageUrl(), holder.photoView);
        }

        @Override
        public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.photo_item, parent, false);
            return new PhotoViewHolder(view);
        }
    }


    private class PhotoObserver extends ContentObserver {
        public PhotoObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            getView().post(new Runnable() {
                @Override
                public void run() {
                    showContent();
                }
            });
        }
    }

    public interface Callbacks {

        void onItemSelected(int id);
    }

}
