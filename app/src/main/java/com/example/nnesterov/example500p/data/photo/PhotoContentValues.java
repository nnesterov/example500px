package com.example.nnesterov.example500p.data.photo;

import java.util.Date;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nnesterov.example500p.data.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code photo} table.
 */
public class PhotoContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return PhotoColumns.CONTENT_URI;
    }

    /**
    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable PhotoSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public PhotoContentValues putPhotoId(@Nullable Integer value) {
        mContentValues.put(PhotoColumns.PHOTO_ID, value);
        return this;
    }

    public PhotoContentValues putPhotoIdNull() {
        mContentValues.putNull(PhotoColumns.PHOTO_ID);
        return this;
    }

    public PhotoContentValues putCategory(@Nullable String value) {
        mContentValues.put(PhotoColumns.CATEGORY, value);
        return this;
    }

    public PhotoContentValues putCategoryNull() {
        mContentValues.putNull(PhotoColumns.CATEGORY);
        return this;
    }

    public PhotoContentValues putName(@Nullable String value) {
        mContentValues.put(PhotoColumns.NAME, value);
        return this;
    }

    public PhotoContentValues putNameNull() {
        mContentValues.putNull(PhotoColumns.NAME);
        return this;
    }

    public PhotoContentValues putUserId(@Nullable Integer value) {
        mContentValues.put(PhotoColumns.USER_ID, value);
        return this;
    }

    public PhotoContentValues putUserIdNull() {
        mContentValues.putNull(PhotoColumns.USER_ID);
        return this;
    }

    public PhotoContentValues putSmallImageUrl(@Nullable String value) {
        mContentValues.put(PhotoColumns.SMALL_IMAGE_URL, value);
        return this;
    }

    public PhotoContentValues putSmallImageUrlNull() {
        mContentValues.putNull(PhotoColumns.SMALL_IMAGE_URL);
        return this;
    }

    public PhotoContentValues putLargeImageUrl(@Nullable String value) {
        mContentValues.put(PhotoColumns.LARGE_IMAGE_URL, value);
        return this;
    }

    public PhotoContentValues putLargeImageUrlNull() {
        mContentValues.putNull(PhotoColumns.LARGE_IMAGE_URL);
        return this;
    }
}
