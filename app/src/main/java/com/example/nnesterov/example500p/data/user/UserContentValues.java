package com.example.nnesterov.example500p.data.user;

import java.util.Date;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nnesterov.example500p.data.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code user} table.
 */
public class UserContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return UserColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable UserSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public UserContentValues putUserId(@Nullable Integer value) {
        mContentValues.put(UserColumns.USER_ID, value);
        return this;
    }

    public UserContentValues putUserIdNull() {
        mContentValues.putNull(UserColumns.USER_ID);
        return this;
    }

    public UserContentValues putName(@Nullable String value) {
        mContentValues.put(UserColumns.NAME, value);
        return this;
    }

    public UserContentValues putNameNull() {
        mContentValues.putNull(UserColumns.NAME);
        return this;
    }

    public UserContentValues putCity(@Nullable String value) {
        mContentValues.put(UserColumns.CITY, value);
        return this;
    }

    public UserContentValues putCityNull() {
        mContentValues.putNull(UserColumns.CITY);
        return this;
    }

    public UserContentValues putCountry(@Nullable String value) {
        mContentValues.put(UserColumns.COUNTRY, value);
        return this;
    }

    public UserContentValues putCountryNull() {
        mContentValues.putNull(UserColumns.COUNTRY);
        return this;
    }
}
