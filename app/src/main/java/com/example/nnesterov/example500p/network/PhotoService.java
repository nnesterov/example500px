package com.example.nnesterov.example500p.network;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;

import com.example.nnesterov.example500p.R;
import com.example.nnesterov.example500p.data.photo.PhotoColumns;
import com.example.nnesterov.example500p.data.photo.PhotoContentValues;
import com.example.nnesterov.example500p.data.user.UserColumns;
import com.example.nnesterov.example500p.data.user.UserContentValues;
import com.example.nnesterov.example500p.network.model.ImageUrl;
import com.example.nnesterov.example500p.network.model.Photo;
import com.example.nnesterov.example500p.network.model.Response;
import com.example.nnesterov.example500p.network.model.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.http.GET;
import retrofit.http.Query;

public class PhotoService extends IntentService {
    public static final String REQUEST_EXTRA = "request";
    public static final String CATEGORY_NAME_EXTRA = "category-name";
    public static final String ID_EXTRA = "id";

    public static final String REQUEST_PHOTO_LIST = "request-photo-list";
    public static final String REQUEST_PHOTO_INFO = "request-photo-info";

    private static final int SMALL_SIZE_ID = 3;  // 280px x 280px
    private static final int LARGE_SIZE_ID = 1080;  // 1080px on large side
    private static final int DEFAULT_COUNT = 100;
    private static final String DEFAULT_FEATURE = "fresh_today";

    private static final String API_ENDPOINT = "https://api.500px.com";
    private static final RestAdapter REST_ADAPTER;
    private static final PhotoServiceInterface PHOTO_SERVICE_INTERFACE;

    static {
        REST_ADAPTER = new RestAdapter.Builder().setEndpoint(API_ENDPOINT).build();
        PHOTO_SERVICE_INTERFACE = REST_ADAPTER.create(PhotoServiceInterface.class);
    }

    public PhotoService() {
        super(PhotoService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String request = intent.getStringExtra(REQUEST_EXTRA);

        if (REQUEST_PHOTO_LIST.equals(request)) {
            requestPhotoList(intent);
        } else if (REQUEST_PHOTO_INFO.equals(request)) {
            // TODO: Some additional info if needed
        } else {
            throw new IllegalArgumentException("Invalid request type " + request);
        }
    }

    private void requestPhotoList(Intent intent) {
        String category = intent.getStringExtra(CATEGORY_NAME_EXTRA);
        String consumerKey = getResources().getString(R.string.consumer_key);
        try {
            Response photoList = PHOTO_SERVICE_INTERFACE.getPhotos(DEFAULT_FEATURE,
                    Arrays.asList(SMALL_SIZE_ID, LARGE_SIZE_ID),
                    consumerKey,
                    category,
                    DEFAULT_COUNT);

            processUsers(photoList.getPhotos());
            processPhotos(photoList.getPhotos(), category);
        } catch (RetrofitError suppressed) {
            // TODO: maybe propagate this error to UI?
            // cannot access network. Do nothing
        }
    }

    private void processPhotos(List<Photo> photoList, String category) {
        ContentValues[] contentValues = new ContentValues[photoList.size()];
        int i = 0;
        for (Photo photo : photoList) {
            PhotoContentValues photoValue = new PhotoContentValues();
            photoValue.putPhotoId(photo.getId());
            photoValue.putName(photo.getName());
            photoValue.putUserId(photo.getUser().getId());
            for (ImageUrl imageUrl : photo.getImages()) {
                if (imageUrl.getSize() == LARGE_SIZE_ID) {
                    photoValue.putLargeImageUrl(imageUrl.getUrl());
                } else if (imageUrl.getSize() == SMALL_SIZE_ID) {
                    photoValue.putSmallImageUrl(imageUrl.getUrl());
                }
            }
            photoValue.putCategory(category);
            contentValues[i] = photoValue.values();
            ++i;
        }
        getContentResolver().bulkInsert(PhotoColumns.CONTENT_URI, contentValues);
        getContentResolver().notifyChange(PhotoColumns.CONTENT_URI, null);
    }

    private void processUsers(List<Photo> photoList) {
        Collection<User> uniqueUsers = getUserList(photoList);
        ContentValues[] contentValues = new ContentValues[uniqueUsers.size()];
        int i = 0;
        for (User uniqueUser : uniqueUsers) {
            UserContentValues userValue = new UserContentValues();
            userValue.putName(uniqueUser.getUsername());
            userValue.putCity(uniqueUser.getCity());
            userValue.putCountry(uniqueUser.getCountry());
            userValue.putUserId(uniqueUser.getId());
            contentValues[i] = userValue.values();
            ++i;
        }

        getContentResolver().bulkInsert(UserColumns.CONTENT_URI, contentValues);
        getContentResolver().notifyChange(UserColumns.CONTENT_URI, null);
    }

    private Collection<User> getUserList(List<Photo> photos) {
        HashMap<Integer, User> users = new HashMap<>();
        for (Photo photo : photos) {
            users.put(photo.getUser().getId(), photo.getUser());
        }
        return users.values();
    }


    private interface PhotoServiceInterface {
        @GET("/v1/photos")
        Response getPhotos(
                @Query("feature") String feature,
                @Query("image_size[]") List<Integer> sizes,
                @Query("consumer_key") String key,
                @Query("only") String category,
                @Query("rpp") int count
        );
    }
}
