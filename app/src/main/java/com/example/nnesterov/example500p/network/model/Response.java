package com.example.nnesterov.example500p.network.model;

import java.util.List;

public class Response {

    private List<Photo> photos;

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }
}
