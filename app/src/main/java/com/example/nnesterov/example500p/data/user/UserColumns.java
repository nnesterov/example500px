package com.example.nnesterov.example500p.data.user;

import android.net.Uri;
import android.provider.BaseColumns;

import com.example.nnesterov.example500p.data.PhotoProvider;
import com.example.nnesterov.example500p.data.photo.PhotoColumns;
import com.example.nnesterov.example500p.data.user.UserColumns;

/**
 * Columns for the {@code user} table.
 */
public class UserColumns implements BaseColumns {
    public static final String TABLE_NAME = "user";
    public static final Uri CONTENT_URI = Uri.parse(PhotoProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String USER_ID = "user_id";

    public static final String NAME = "name";

    public static final String CITY = "city";

    public static final String COUNTRY = "country";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            USER_ID,
            NAME,
            CITY,
            COUNTRY
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(USER_ID) || c.contains("." + USER_ID)) return true;
            if (c.equals(NAME) || c.contains("." + NAME)) return true;
            if (c.equals(CITY) || c.contains("." + CITY)) return true;
            if (c.equals(COUNTRY) || c.contains("." + COUNTRY)) return true;
        }
        return false;
    }

}
