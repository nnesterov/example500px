package com.example.nnesterov.example500p.data.user;

import java.util.Date;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.example.nnesterov.example500p.data.base.AbstractSelection;

/**
 * Selection for the {@code user} table.
 */
public class UserSelection extends AbstractSelection<UserSelection> {
    @Override
    protected Uri baseUri() {
        return UserColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @param sortOrder How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort
     *            order, which may be unordered.
     * @return A {@code UserCursor} object, which is positioned before the first entry, or null.
     */
    public UserCursor query(ContentResolver contentResolver, String[] projection, String sortOrder) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), sortOrder);
        if (cursor == null) return null;
        return new UserCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null)}.
     */
    public UserCursor query(ContentResolver contentResolver, String[] projection) {
        return query(contentResolver, projection, null);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, projection, null, null)}.
     */
    public UserCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null, null);
    }


    public UserSelection id(long... value) {
        addEquals("user." + UserColumns._ID, toObjectArray(value));
        return this;
    }

    public UserSelection userId(Integer... value) {
        addEquals(UserColumns.USER_ID, value);
        return this;
    }

    public UserSelection userIdNot(Integer... value) {
        addNotEquals(UserColumns.USER_ID, value);
        return this;
    }

    public UserSelection userIdGt(int value) {
        addGreaterThan(UserColumns.USER_ID, value);
        return this;
    }

    public UserSelection userIdGtEq(int value) {
        addGreaterThanOrEquals(UserColumns.USER_ID, value);
        return this;
    }

    public UserSelection userIdLt(int value) {
        addLessThan(UserColumns.USER_ID, value);
        return this;
    }

    public UserSelection userIdLtEq(int value) {
        addLessThanOrEquals(UserColumns.USER_ID, value);
        return this;
    }

    public UserSelection name(String... value) {
        addEquals(UserColumns.NAME, value);
        return this;
    }

    public UserSelection nameNot(String... value) {
        addNotEquals(UserColumns.NAME, value);
        return this;
    }

    public UserSelection nameLike(String... value) {
        addLike(UserColumns.NAME, value);
        return this;
    }

    public UserSelection nameContains(String... value) {
        addContains(UserColumns.NAME, value);
        return this;
    }

    public UserSelection nameStartsWith(String... value) {
        addStartsWith(UserColumns.NAME, value);
        return this;
    }

    public UserSelection nameEndsWith(String... value) {
        addEndsWith(UserColumns.NAME, value);
        return this;
    }

    public UserSelection city(String... value) {
        addEquals(UserColumns.CITY, value);
        return this;
    }

    public UserSelection cityNot(String... value) {
        addNotEquals(UserColumns.CITY, value);
        return this;
    }

    public UserSelection cityLike(String... value) {
        addLike(UserColumns.CITY, value);
        return this;
    }

    public UserSelection cityContains(String... value) {
        addContains(UserColumns.CITY, value);
        return this;
    }

    public UserSelection cityStartsWith(String... value) {
        addStartsWith(UserColumns.CITY, value);
        return this;
    }

    public UserSelection cityEndsWith(String... value) {
        addEndsWith(UserColumns.CITY, value);
        return this;
    }

    public UserSelection country(String... value) {
        addEquals(UserColumns.COUNTRY, value);
        return this;
    }

    public UserSelection countryNot(String... value) {
        addNotEquals(UserColumns.COUNTRY, value);
        return this;
    }

    public UserSelection countryLike(String... value) {
        addLike(UserColumns.COUNTRY, value);
        return this;
    }

    public UserSelection countryContains(String... value) {
        addContains(UserColumns.COUNTRY, value);
        return this;
    }

    public UserSelection countryStartsWith(String... value) {
        addStartsWith(UserColumns.COUNTRY, value);
        return this;
    }

    public UserSelection countryEndsWith(String... value) {
        addEndsWith(UserColumns.COUNTRY, value);
        return this;
    }
}
