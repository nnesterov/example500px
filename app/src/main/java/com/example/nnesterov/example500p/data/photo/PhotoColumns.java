package com.example.nnesterov.example500p.data.photo;

import android.net.Uri;
import android.provider.BaseColumns;

import com.example.nnesterov.example500p.data.PhotoProvider;
import com.example.nnesterov.example500p.data.photo.PhotoColumns;
import com.example.nnesterov.example500p.data.user.UserColumns;

/**
 * Columns for the {@code photo} table.
 */
public class PhotoColumns implements BaseColumns {
    public static final String TABLE_NAME = "photo";
    public static final Uri CONTENT_URI = Uri.parse(PhotoProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String PHOTO_ID = "photo_id";

    public static final String CATEGORY = "category";

    public static final String NAME = "name";

    public static final String USER_ID = "user_id";

    public static final String SMALL_IMAGE_URL = "small_image_url";

    public static final String LARGE_IMAGE_URL = "large_image_url";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            PHOTO_ID,
            CATEGORY,
            NAME,
            USER_ID,
            SMALL_IMAGE_URL,
            LARGE_IMAGE_URL
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(PHOTO_ID) || c.contains("." + PHOTO_ID)) return true;
            if (c.equals(CATEGORY) || c.contains("." + CATEGORY)) return true;
            if (c.equals(NAME) || c.contains("." + NAME)) return true;
            if (c.equals(USER_ID) || c.contains("." + USER_ID)) return true;
            if (c.equals(SMALL_IMAGE_URL) || c.contains("." + SMALL_IMAGE_URL)) return true;
            if (c.equals(LARGE_IMAGE_URL) || c.contains("." + LARGE_IMAGE_URL)) return true;
        }
        return false;
    }

}
