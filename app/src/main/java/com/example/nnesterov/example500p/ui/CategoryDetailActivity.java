package com.example.nnesterov.example500p.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.nnesterov.example500p.R;

public class CategoryDetailActivity extends ActionBarActivity implements CategoryDetailFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putString(CategoryDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(CategoryDetailFragment.ARG_ITEM_ID));
            CategoryDetailFragment fragment = new CategoryDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.category_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, CategoryListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(int id) {
        Fragment photoFragment = new PhotoDetailsFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(PhotoDetailsFragment.ARG_PHOTO_ID, id);
        photoFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.category_detail_container, photoFragment)
                .addToBackStack("")
                .commit();
    }
}
