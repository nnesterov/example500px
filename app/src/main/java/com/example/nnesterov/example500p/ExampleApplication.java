package com.example.nnesterov.example500p;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ExampleApplication extends Application {
    @Override
    public void onCreate() {
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .build();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(this).
                defaultDisplayImageOptions(displayImageOptions)
                .build();
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
    }
}
